package ru.mygame.utils;

public class Time {

    public static final Long SECOND = 1000000000l;

    public static long get(){
        return System.nanoTime();
    }

}
